﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ToDoList_Domain.Models;
using ToDoList_Domain.Data;
using ToDoList_Domain.Repository;

namespace ToDoListWebApplication.Controllers
{
    public class ToDoEntriesController : Controller
    {
        private readonly ToDoDbContext _context;
        private readonly IToDoRepository _repository;
        public ToDoEntriesController(ToDoDbContext context, IToDoRepository repository)
        {
            _context = context;
            _repository = repository;
        }

        // GET: ToDoEntries
        public async Task<IActionResult> Index()
        {
            var toDoListWebApplicationContext = _context.ToDoEntries.Include(t => t.ToDoList);
            return View(await toDoListWebApplicationContext.ToListAsync());
        }

        // GET: ToDoEntries/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var toDoEntries = await _context.ToDoEntries
                .Include(t => t.ToDoList)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoEntries == null)
            {
                return NotFound();
            }

            return View(toDoEntries);
        }

        // GET: ToDoEntries/Create
        public IActionResult Create()
        {
            ViewData["ToDoListId"] = new SelectList(_context.Set<ToDoList>(), "Id", "ToDoListDescription");
            return View();
        }

        // POST: ToDoEntries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ToDoListId,Name,Description,Status,CreatedTime,ModifiedTime,AdditionalNotes,ReminderDate,DueDate,Hiden")] ToDoEntries toDoEntries)
        {
            if (!ModelState.IsValid)
            {
                await _repository.CreateEntryAsync(toDoEntries);
                return RedirectToAction(nameof(Index));
            }
            ViewData["ToDoListId"] = new SelectList(_context.Set<ToDoList>(), "Id", "ToDoListDescription", toDoEntries.ToDoListId);
            return View(toDoEntries);
        }

        // GET: ToDoEntries/Edit/5
        public async Task<IActionResult> Edit(int id)
        {

            var toDoEntries = await _context.ToDoEntries.FindAsync(id);
            if (toDoEntries == null)
            {
                return NotFound();
            }
            ViewData["ToDoListId"] = new SelectList(_context.Set<ToDoList>(), "Id", "ToDoListDescription", toDoEntries.ToDoListId);
            return View(toDoEntries);
        }

        // POST: ToDoEntries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ToDoListId,Name,Description,Status,ModifiedTime,AdditionalNotes,ReminderDate,DueDate,Hiden")] ToDoEntries toDoEntries)
        {

            if (!ModelState.IsValid)
            {
                try
                {
                    await _repository.UpdateEntryAsync(toDoEntries);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToDoEntriesExists(toDoEntries.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ToDoListId"] = new SelectList(_context.Set<ToDoList>(), "Id", "ToDoListDescription", toDoEntries.ToDoListId);
            return View(toDoEntries);
        }

        // GET: ToDoEntries/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            var toDoEntries = await _context.ToDoEntries
                .Include(t => t.ToDoList)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoEntries == null)
            {
                return NotFound();
            }

            return View(toDoEntries);
        }

        // POST: ToDoEntries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _repository.RemoveEntryAsync(id);
            return RedirectToAction(nameof(Index));
        }

        private bool ToDoEntriesExists(int id)
        {
            return (_context.ToDoEntries?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        public async Task<IActionResult> EntriesDueToday()
        {
            var entriesDueToday = await _repository.GetEntriesDueTodayAsync();
            return View("Index", entriesDueToday);
        }

        public async Task<IActionResult> GetEntriesByListId(int id)
        {
            var entries = await _repository.GetEntriesByListIdAsync(id);

            if (entries == null)
            {
                return NotFound();
            }

            return View(entries);
        }

        public async Task<IActionResult> CheckReminder()
        {
            var reminderCount = await _repository.CheckReminderAsync();
            return Json(reminderCount);
        }
    }
}