﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDoList_Domain.Models;
using ToDoList_Domain.Data;
using ToDoList_Domain.Repository;

namespace ToDoListWebApplication.Controllers
{
    public class ToDoListsController : Controller
    {
        private readonly ToDoDbContext _context;
        private readonly IToDoRepository _repository;
        public ToDoListsController(ToDoDbContext context, IToDoRepository repository)
        {
            _context = context;
            _repository = repository;
        }

        // GET: ToDoLists
        public async Task<IActionResult> Index()
        {
            return _context.ToDoLists != null ?
                        View(await _context.ToDoLists.ToListAsync()) :
                        Problem("Entity set 'ToDoListsDbContext.ToDoList'  is null.");
        }

        // GET: ToDoLists/Details/5
        public async Task<IActionResult> Details(int id)
        {

            var toDoList = await _context.ToDoLists
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoList == null)
            {
                return NotFound();
            }

            return View(toDoList);
        }

        // GET: ToDoLists/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ToDoLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ToDoListName,ToDoListDescription,IsHidden")] ToDoList toDoList)
        {
            if (!ModelState.IsValid)
            {
                await _repository.CreateListAsync(toDoList);
                return RedirectToAction(nameof(Index));
            }
            return View(toDoList);
        }

        // GET: ToDoLists/Edit/5
        public async Task<IActionResult> Edit(int id)
        {

            var toDoList = await _context.ToDoLists.FindAsync(id);
            if (toDoList == null)
            {
                return NotFound();
            }
            return View(toDoList);
        }

        // POST: ToDoLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ToDoListName,ToDoListDescription,IsHidden")] ToDoList toDoList)
        {
            if (id != toDoList.Id)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                try
                {
                    _context.Update(toDoList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToDoListExists(toDoList.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(toDoList);
        }

        // GET: ToDoLists/Delete/5
        public async Task<IActionResult> Delete(int id)
        {

            var toDoList = await _context.ToDoLists
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoList == null)
            {
                return NotFound();
            }

            return View(toDoList);
        }

        // POST: ToDoLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _repository.GetEntriesDueTodayAsync(id);
            return RedirectToAction(nameof(Index));
        }

        private bool ToDoListExists(int id)
        {
            return (_context.ToDoLists?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        [HttpPost, ActionName("CopyList")]
        public async Task<IActionResult> CopyList(int id)
        {
            try 
            {
                await _repository.CopyListAsync(id);
                return Ok("List copied");
            }
            catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
