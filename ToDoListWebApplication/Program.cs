﻿using Microsoft.AspNetCore.Routing.Constraints;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;
using ToDoList_Domain.Data;
using ToDoList_Domain.Repository;

namespace ToDoListWebApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);           
            builder.Services.AddDbContext<ToDoDbContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("ToDoDbContext") ?? throw new InvalidOperationException("Connection string 'ToDoDbContext' not found.")));

            // Add services to the container.
            builder.Services.AddControllersWithViews();
            builder.Services.AddScoped<IToDoRepository, ToDoRepository>();
            builder.Services.TryAddScoped<IToDoRepository, ToDoRepository>();
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=ToDoLists}/{action=Index}/{id?}");
 
             app.MapControllerRoute(
                  name: "copyList",
                  pattern: "ToDoLists/CopyListAsync/{id}",
                  defaults: new { controller = "ToDoLists", action = "CopyListAsync" },
                  constraints: new { httpMethod = new HttpMethodRouteConstraint("POST") });
        
            app.Run();
        }
    }
}