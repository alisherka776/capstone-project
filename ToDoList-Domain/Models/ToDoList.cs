﻿namespace ToDoList_Domain.Models
{
    public class ToDoList
    {
        public int Id { get; set; }

        public string ToDoListName { get; set; } = string.Empty;

        public string ToDoListDescription { get; set; } = string.Empty;

        public DateTime CreationTime { get; set; }

        public bool IsHidden { get; set; }

        public List<ToDoEntries>? ToDoEntries { get; set; }
    }
}