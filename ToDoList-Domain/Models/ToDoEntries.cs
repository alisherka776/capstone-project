﻿using Microsoft.Build.Framework;

namespace ToDoList_Domain.Models
{
    public enum Status
    {
        Completed,
        InProgress,
        NotStarted
    }
    public class ToDoEntries
    {
        public int Id { get; set; }

        public int ToDoListId { get; set; }

        public ToDoList ToDoList { get; set; } = null!;

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public Status Status { get; set; }

        public DateTime? CreatedTime { get; set; }

        public DateTime? ModifiedTime { get; set; }
        
        public string AdditionalNotes { get; set; } = string.Empty;

        public DateTime? ReminderDate { get; set; }

        public DateTime? DueDate { get; set; }

        public bool Hiden { get; set; }
    }
}
