﻿using Microsoft.EntityFrameworkCore;
using ToDoList_Domain.Models;

namespace ToDoList_Domain.Data
{
    public class ToDoDbContext : DbContext
    {
        public virtual DbSet<ToDoList> ToDoLists { get; set; }
        public virtual DbSet<ToDoEntries> ToDoEntries { get; set; }
        public ToDoDbContext(DbContextOptions<ToDoDbContext> options) : base(options) 
        { 

        }

        public ToDoDbContext() : base()
        {
        
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ToDoList>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.ToDoListName).IsRequired();
                entity.HasMany(e => e.ToDoEntries).WithOne(e => e.ToDoList).HasForeignKey(t => t.ToDoListId);
            });

            modelBuilder.Entity<ToDoEntries>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Name).IsRequired();
                entity.Property(e => e.Description).IsRequired(false);
                entity.Property(e => e.DueDate).IsRequired(false);
                entity.Property(e => e.CreatedTime).IsRequired(false);
                entity.Property(e => e.ModifiedTime).IsRequired(false);
                entity.Property(e => e.Status).IsRequired();
                entity.Property(e => e.AdditionalNotes).IsRequired(false);
                entity.HasOne(e => e.ToDoList).WithMany(l => l.ToDoEntries).HasForeignKey(l => l.ToDoListId);
            });
        }
    }
}
