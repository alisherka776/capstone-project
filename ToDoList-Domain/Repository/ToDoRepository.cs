﻿using Microsoft.EntityFrameworkCore;
using ToDoList_Domain.Data;
using ToDoList_Domain.Models;

namespace ToDoList_Domain.Repository
{
    public class ToDoRepository : IToDoRepository
    {
        private readonly ToDoDbContext _context;

        public ToDoRepository(ToDoDbContext context)
        {
            _context = context;
        }

        public async Task CopyListAsync(int id)
        {
            var listToCopy = await GetListByIdAsync(id);

            var newToDoList = new ToDoList
            {
                ToDoListName = listToCopy.ToDoListName,
                ToDoListDescription = listToCopy.ToDoListDescription,
                CreationTime = DateTime.Now,
                IsHidden = listToCopy.IsHidden,

            };
            if (listToCopy.ToDoEntries is null) { return; }

                foreach (var entry in listToCopy.ToDoEntries)
                {
                    var newEntry = new ToDoEntries
                    {

                        Name = entry.Name,
                        Description = entry.Description,
                        Status = entry.Status,
                        CreatedTime = DateTime.Now,
                        ModifiedTime = DateTime.Now,
                        AdditionalNotes = entry.AdditionalNotes,
                        ReminderDate = entry.ReminderDate,
                        DueDate = entry.DueDate,
                        Hiden = entry.Hiden
                    };

                    newToDoList.ToDoEntries?.Add(newEntry);
                }

            _context.ToDoLists.Add(newToDoList);
            await _context.SaveChangesAsync();
        }

        public async Task CreateEntryAsync(ToDoEntries entry)
        {
            entry.CreatedTime = DateTime.Now;
            entry.ModifiedTime = DateTime.Now;
            await _context.ToDoEntries.AddAsync(entry);
            await _context.SaveChangesAsync();
        }

        public async Task CreateListAsync(ToDoList list)
        {
            list.CreationTime = DateTime.Now;
            await _context.ToDoLists.AddAsync(list);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<ToDoList>> GetAllListsAsync()
        {
            return await _context.ToDoLists.ToListAsync();
        }

        public async Task<IEnumerable<ToDoEntries>> GetEntriesDueTodayAsync()
        {
            var today = DateTime.Today;

            return await _context.ToDoEntries
                .Where(e => e.DueDate.HasValue &&
                            e.DueDate.Value.Date == today.Date)
                .ToListAsync();
        }

        public async Task<int> CheckReminderAsync()
        {
            var today = DateTime.Today;
            return await _context.ToDoEntries
                .Where(e => e.ReminderDate.HasValue && 
                            e.ReminderDate.Value.Date == today.Date)
                .CountAsync();
        }

        public async Task<ToDoEntries> GetEntryByIdAsync(int id)
        {
            return await _context.ToDoEntries.FindAsync(id) ?? throw new NullReferenceException($"{id} not found.");
        }

        public async Task<ToDoList> GetListByIdAsync(int id)
        {
            return await _context.ToDoLists.FindAsync(id) ?? throw new NullReferenceException($"{id} not found.");
        }

        public async Task RemoveEntryAsync(int id)
        {
            var entry = await GetEntryByIdAsync(id);

            _context.ToDoEntries.Remove(entry);
            await _context.SaveChangesAsync();
        }

        public async Task GetEntriesDueTodayAsync(int id)
        {
           var list = await GetListByIdAsync(id);

            _context.ToDoLists.Remove(list);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateEntryAsync(ToDoEntries entry)
        {
            var existingEntry = await GetEntryByIdAsync(entry.Id); 

            if (existingEntry != null)
            {
                existingEntry.ToDoList = entry.ToDoList;
                existingEntry.ToDoListId = entry.ToDoListId;
                existingEntry.ReminderDate = entry.ReminderDate;
                existingEntry.DueDate = entry.DueDate;
                existingEntry.Name = entry.Name;
                existingEntry.AdditionalNotes = entry.AdditionalNotes;
                existingEntry.Description = entry.Description;
                existingEntry.CreatedTime = entry.CreatedTime;
                existingEntry.ModifiedTime = DateTime.Now;
                existingEntry.Status = entry.Status;
                existingEntry.Hiden = entry.Hiden;
            }
            else
            {
                throw new ArgumentException("Entry was null");
            }
            _context.ToDoEntries.Update(existingEntry);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateListAsync(ToDoList list)
        {
            _context.ToDoLists.Update(list);
            await _context.SaveChangesAsync();
        }

        public async Task<List<ToDoEntries>> GetEntriesByListIdAsync(int id)
        {
            var entries = await _context.ToDoEntries
                .Where(e => e.ToDoListId == id)
                .ToListAsync();

            return entries;
        }
    }
}
