﻿using ToDoList_Domain.Models;

namespace ToDoList_Domain.Repository
{
    public interface IToDoRepository
    {
        Task<IEnumerable<ToDoList>> GetAllListsAsync();
        Task<ToDoList> GetListByIdAsync(int id);
        Task CreateListAsync(ToDoList list);
        Task UpdateListAsync(ToDoList list);
        Task<ToDoEntries> GetEntryByIdAsync(int id);
        Task CreateEntryAsync(ToDoEntries entry);
        Task UpdateEntryAsync(ToDoEntries entry);
        Task<IEnumerable<ToDoEntries>> GetEntriesDueTodayAsync();
        Task CopyListAsync(int id);
        Task GetEntriesDueTodayAsync(int id);
        Task RemoveEntryAsync(int id);
        Task<int> CheckReminderAsync();
        Task<List<ToDoEntries>> GetEntriesByListIdAsync(int id);
    }
}
